/*
* GL01Hello.cpp: Test OpenGL C/C++ Setup
*/
#include <stdio.h>
#include <freeglut.h>  // GLUT, includes glu.h and gl.h

#define N 10000000


double a[4] = {0.0,0.2,-0.15,0.8};
double b[4] = {0.0,-0.26,0.28,0.04};
double c[4] = {0.0,0.23,0.26,-0.04};
double d[4] = {0.16,0.22,0.24,0.85};
double e[4] = {-0.05,0.0,0.0,0.0};
double f[4] = {0.0,1.6,0.44,1.6};


/* DRAKE
double a[2] = { 0.824074, 0.088272 };
double b[2] = { 0.281428, 0.520988 };
double c[2] = { -0.212346, -0.463889 };
double d[2] = { 0.864198, -0.377778 };
double e[2] = { -1.882290, 0.785360 };
double f[2] = { -0.110607, 8.095795 };
*/

/* NUBES (mio)
double a[2] = {0.8, 0.02};
double b[2] = {-0.1, 0.5};
double c[2] = {0.4, -0.58};
double d[2] = {0.86, -0.1};
double e[2] = {-1.2, 0.4};
double f[2] = {-0.2, 5};
*/

/* SPIRAL
double a[3] = {0.787879, -0.121212, 0.181818};
double b[3] = {-0.424242, 0.257576, -0.136364};
double c[3] = {0.242424, 0.151515, 0.090909};
double d[3] = {0.859848, 0.053030, 0.181818};
double e[3] = {1.758647, -6.721654, 6.086107};
double f[3] = {1.408065, 1.377236, 1.568035};
*/

struct pixel {
	GLfloat x;
	GLfloat y;
};

bool drawed = false;

double r;
double yn, xn;

int scale = 85;
int xdif = 1000;
int ydif = 100;

int k, n = 0, percent = (N / 100);

float x = 0, y = 0;

struct pixel pixels[N * 2];

void nextPoint() {


	while (n<N) {
		r = rand() % 100;

		
		if (r < 1)
		k = 0;
		else if (r < 8)
		k = 1;
		else if (r < 15)
		k = 2;
		else
		k = 3;
		
		/* DRAKE
		if (r < 80)
			k = 0;
		else
			k = 1;
		*/
		/* NUBES (mio)
		if(r < 60)
		k = 0;
		else
		k = 1;
		*/
		/* SPIRAL
		if(r < 90)
		k = 0;
		else if (r < 95)
		k = 1;
		else
		k = 2;
		*/
		
		xn = a[k] * x + b[k] * y + e[k];
		yn = c[k] * x + d[k] * y + f[k];
		pixels[n].x = (xn * scale) + xdif;
		pixels[n].y = (yn * scale) + ydif;
		x = xn;
		y = yn;
		if (n % percent == 0) {
			printf("%d %\n", (100*n)/N);
		}
		n++;

	}
}

void display() {

	//glClear(GL_COLOR_BUFFER_BIT);         // Clear the color buffer
	if(!drawed){
		glDrawArrays(GL_POINTS, 0, N);
		drawed = true;
		glutSwapBuffers();
	}
}

void idle() {
	glutPostRedisplay();
}

void resize(int w, int h) {
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, (GLdouble)w, 0, (GLdouble)h);
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);                 // Initialize GLUT
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(1920, 1080);   // Set the window's initial width & height
	glutInitWindowPosition(0, 0);
	glutCreateWindow("OpenGL Setup Test"); // Create a window with the given title
	glColor3f(1.0, 1.0, 1.0);
	glutIdleFunc(idle);
	glutReshapeFunc(resize);
	glutDisplayFunc(display); // Register display callback handler for window re-paint
	glEnableClientState(GL_VERTEX_ARRAY);
	nextPoint();
	glVertexPointer(2, GL_FLOAT, sizeof(pixel), pixels);
	glutMainLoop();           // Enter the infinitely event-processing loop
	return 0;
}